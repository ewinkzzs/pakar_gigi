<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penyakit_model extends CI_Model
{

	var $table = 'penyakit'; 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	public function get_all()
	{
	$this->db->from('penyakit');
	$query=$this->db->get();
	return $query->result();
	}


	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id_penyakit',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function penyakit_update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function penyakit_add($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id_penyakit', $id);
		$this->db->delete($this->table);
	}


}