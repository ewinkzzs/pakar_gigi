<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	public function get_count_gejala()
	{
		$results = array();
        $query = $this->db->query('SELECT COUNT(id_gejala) as jumlah FROM gejala');
        return $query->row();
	}

	public function get_count_penyakit()
	{
		$results = array();
        $query = $this->db->query('SELECT COUNT(id_penyakit) as jumlah FROM penyakit');
        return $query->row();
	}

	public function get_count_solusi()
	{
		$results = array();
        $query = $this->db->query('SELECT COUNT(id) as jumlah FROM solusi');
        return $query->row();
	}

	public function get_count_pasien()
	{
		$results = array();
        $query = $this->db->query('SELECT COUNT(id)-1 as jumlah FROM users');
        return $query->row();
	}


}