<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diagnosa_model extends CI_Model
{


    function __construct() {
        
    }


	public function hasil_add($data)
	{
		$this->db->insert('hasil', $data);
		return $this->db->insert_id();
	}

    public function hasil_update($data,$where)
    {
        $this->db->update('hasil', $data, $where);
        return $this->db->affected_rows();
    }

	public function tmp_gejala_delete()
	{
		$this->db->empty_table('tmp_gejala');
	}

	public function tmp_penyakit_delete()
	{
		$this->db->empty_table('tmp_penyakit');
	}

	public function tmp_gejala_add($data)
	{
		$this->db->insert('tmp_gejala', $data);
		return $this->db->insert_id();
	}

 	function get_list_by_id($id){
        $results = array();
        $query = $this->db->query('SELECT id_gejala,kd_gejala,gejala from gejala where id_gejala in ('.$id.')');
         if ($query->num_rows() > 0) {
                return $query->result();
 
         return $results;
     }
     }

     function get_bobot_group(){
        $results = array();
        $query = $this->db->query('SELECT * FROM bobot GROUP BY kd_penyakit');
        return $query->result();
        
     }

     function get_bobot_where2($id){
         $results = array();
        $query = $this->db->query('SELECT * FROM bobot WHERE kd_penyakit="'.$id.'"');
        return $query->result();
     }

     function get_bobot_where($id){
         $results = array();
        $query = $this->db->query('SELECT * FROM bobot WHERE kd_penyakit="'.$id.'"');
        return $query->row();
     }

     function get_sum_bobot($id){
         $results = array();
        $query = $this->db->query('SELECT sum(bobot) AS jumlahbobot from bobot where kd_penyakit="'.$id.'"');
         return $query->row();
     }

     function get_tmp_gejala_where($id){
         $results = array();
        $query = $this->db->query('SELECT * FROM tmp_gejala WHERE id_gejala="'.$id.'"');
        return $query->row();
     }

     public function tmp_penyakit_add($data)
	 {
		$this->db->insert('tmp_penyakit', $data);
		return $this->db->insert_id();
	 }

     function get_max_tmp_penyakit2(){
         $results = array();
        $query = $this->db->query('SELECT kd_penyakit,MAX(cf) AS cf,jml_gejala AS max_jml_gejala FROM tmp_penyakit GROUP BY cf  ORDER BY cf DESC');
        return $query->row();
     }

     function get_max_tmp_penyakit(){
         $results = array();
        $query = $this->db->query('SELECT kd_penyakit,jml_gejala,cf FROM tmp_penyakit ORDER BY jml_gejala DESC,cf DESC');
        return $query->row();
     }

     function get_max_penyakit_tmp_penyakit($gejala,$cf){
        $results = array();
        $query = $this->db->query('SELECT kd_penyakit,jml_gejala,cf FROM tmp_penyakit WHERE jml_gejala='.$gejala.' AND cf='.$cf.'');
        return $query->result();
     }

     function get_penyakit_where($id){
        $results = array();
        $query = $this->db->query('SELECT * FROM penyakit WHERE kd_penyakit="'.$id.'"');
        return $query->row();
     }

     function get_gejala(){
        $results = array();
        $query = $this->db->query('SELECT gejala.id_gejala,gejala.gejala AS namagejala,tmp_gejala.id_gejala FROM gejala INNER JOIN tmp_gejala ON gejala.id_gejala = tmp_gejala.id_gejala');
         if ($query->num_rows() > 0) {
                return $query->result();
 
        return $results;
        }
     }


     function get_hasil(){
        $results = array();
        $query = $this->db->query('SELECT
                                    hasil.id,
                                    hasil.id_pasien,
                                    users.nama,
                                    users.jk,
                                    hasil.kd_penyakit,
                                    penyakit.nama_penyakit,
                                    hasil.cf,
                                    hasil.tanggal,
                                    hasil.gejala,
                                    hasil.waktu
                                    FROM
                                    hasil
                                    INNER JOIN users ON hasil.id_pasien = users.id
                                    INNER JOIN penyakit ON hasil.kd_penyakit = penyakit.kd_penyakit');
        return $query->result();
     }

     function get_hasil_desc(){
        $results = array();
        $query = $this->db->query('SELECT *FROM hasil ORDER BY id DESC');
        return $query->row();
     }

     function get_hasil_where($id){
        $results = array();
        $query = $this->db->query('SELECT *FROM hasil where id="'.$id.'"');
        return $query->row();
     }

     function get_solusi($id){
        $results = array();
        $query = $this->db->query('SELECT *FROM solusi where id_penyakit="'.$id.'"');
        return $query->row();
     }

    public function get_all_penyakit()
    {
        $this->db->from('penyakit');
        $query=$this->db->get();
        return $query->result();
    }

    function get_rule($id,$gejala){
        $results = array();
        $query = $this->db->query(' SELECT
                                    rule.id,
                                    rule.kd_gejala,
                                    gejala.gejala,
                                    rule.kd_penyakit,
                                    penyakit.nama_penyakit,
                                    rule.md,
                                    rule.mb
                                    FROM
                                    rule
                                    INNER JOIN penyakit ON rule.kd_penyakit = penyakit.kd_penyakit
                                    INNER JOIN gejala ON rule.kd_gejala = gejala.id_gejala 
                                    WHERE rule.kd_penyakit="'.$id.'" and rule.kd_gejala in ('.$gejala.')');
        return $query->result();
     }

     function get_row_rule($id,$gejala){
        $results = array();
        $query = $this->db->query(' SELECT
                                    rule.id,
                                    rule.kd_gejala,
                                    gejala.gejala,
                                    rule.kd_penyakit,
                                    penyakit.nama_penyakit,
                                    rule.md,
                                    rule.mb
                                    FROM
                                    rule
                                    INNER JOIN penyakit ON rule.kd_penyakit = penyakit.kd_penyakit
                                    INNER JOIN gejala ON rule.kd_gejala = gejala.id_gejala 
                                    WHERE rule.kd_penyakit="'.$id.'" and rule.kd_gejala in ('.$gejala.')');
        return $query;
     }

     function get_jml_gejala($id,$gejala){
        $results = array();
        $query = $this->db->query(' SELECT
                                    rule.id,
                                    rule.kd_penyakit,
                                    COUNT(id) AS jml_gejala                                                                     
                                    FROM
                                    rule
                                    WHERE kd_penyakit="'.$id.'" and kd_gejala in ('.$gejala.')');
        return $query->row();
     }

}