<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hasil_model extends CI_Model
{


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	public function get_all()
	{	
     	$results = array();
        $query = $this->db->query('SELECT hasil.id AS id,hasil.id_pasien AS id_pasien,
									users.nama AS nama,	users.jk AS jk, hasil.kd_penyakit AS kd_penyakit,
									penyakit.nama_penyakit AS nama_penyakit,hasil.waktu AS waktu,
									hasil.tanggal AS tanggal FROM	hasil	JOIN penyakit ON 
									hasil.kd_penyakit = penyakit.kd_penyakit
									JOIN users ON hasil.id_pasien = users.id');
        return $query->result();
	}

	public function get_export($tgl1,$tgl2)
	{	
     	$results = array();
        $query = $this->db->query('SELECT hasil.id AS id,hasil.id_pasien AS id_pasien,
									users.nama AS nama,	users.jk AS jk, hasil.kd_penyakit AS kd_penyakit,
									penyakit.nama_penyakit AS nama_penyakit,hasil.waktu AS waktu,
									hasil.tanggal AS tanggal,users.no_telp,users.pekerjaan
									FROM	hasil	JOIN penyakit ON 
									hasil.kd_penyakit = penyakit.kd_penyakit
									JOIN users ON hasil.id_pasien = users.id
									WHERE tanggal BETWEEN "'.$tgl1.'" AND "'.$tgl2.'"
									');
        return $query->result();
	}


	public function get_by_id($id)
	{

		$results = array();
        $query = $this->db->query('SELECT hasil.id AS id,hasil.id_pasien AS id_pasien,
									users.nama AS nama,	hasil.kd_penyakit AS kd_penyakit,
									penyakit.nama_penyakit AS nama_penyakit,hasil.waktu AS waktu,
									hasil.tanggal AS tanggal FROM	hasil	JOIN penyakit ON 
									hasil.kd_penyakit = penyakit.kd_penyakit
									JOIN users ON hasil.id_pasien = users.id WHERE hasil.id_pasien="' . $id . '" order by hasil.waktu DESC');
   
         return $query->result();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id_gejala', $id);
		$this->db->delete($this->table);
	}


}