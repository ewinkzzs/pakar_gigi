<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gejala_model extends CI_Model
{

	var $table = 'gejala'; 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	public function get_all()
	{
	$this->db->from('gejala');
	$query=$this->db->get();
	return $query->result();
	}


	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id_gejala',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function gejala_update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function gejala_add($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id_gejala', $id);
		$this->db->delete($this->table);
	}


}