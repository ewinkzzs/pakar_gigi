<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solusi_model extends CI_Model
{

	var $table = 'solusi'; 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	public function get_all()
	{
		$results = array();
		$query = $this->db->query(' SELECT
									solusi.id,
									solusi.id_penyakit,
									penyakit.nama_penyakit,
									penyakit.gambar,
									solusi.defenisi,
									solusi.solusi
									FROM
									solusi
									INNER JOIN penyakit ON solusi.id_penyakit = penyakit.id_penyakit
									');
		return $query->result();

	}


	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function update_($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function add_($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function delete_($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}


}