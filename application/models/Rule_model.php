<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rule_model extends CI_Model
{

	var $table = 'rule'; 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	public function get_all()
	{
		$this->db->from('rule');
		$query=$this->db->get();
		return $query->result();
	}

	public function get_where($id)
	{
		$results = array();
		$query = $this->db->query(' SELECT
									rule.id,
									rule.kd_gejala,
									gejala.gejala,
									rule.kd_penyakit,
									rule.md,
									rule.mb
									FROM
									rule
									INNER JOIN gejala ON rule.kd_gejala = gejala.id_gejala
									WHERE kd_penyakit="'.$id.'"
									');
		return $query->result();
	}


	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function update_($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function add_($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function delete_($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}


}