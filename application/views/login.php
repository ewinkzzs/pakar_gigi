<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, maximum-scale=1">
<title>Admin SPKPUKDI BPTP</title>
<link rel="icon" href="<?php echo base_url();?>assets/images/logo.png">
<!-- <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"> -->
<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css"> 
<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet" type="text/css"> 
<link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/datatables/css/dataTables.bootstrap.css')?>" rel="stylesheet">
<script href="<?php echo base_url(); ?>assets/js/datepicker.js"></script>
 
 
</head>
<body>
<div>
  <?php  
    //Cetak jika ada notifikasi 
  	if($this->session->flashdata('sukses')) {  
  		echo '<p class="warning" style="margin: 10px 20px;">'.$this->session->flashdata('sukses').'</p>';  
  	}  
   ?> 
<?php echo form_open('login');?>  
</div>  

<br> <br> <br> <br> <br>             
<section>
  <div class="container">
    <div class="service_wrapper">
      <div class="row">
        <div class="col-lg-4">
          
        </div>

        <div class="col-lg-4 border">			
          <div class="service_block">
          <div class="modal-content">
          	<div class="panel-body">              
            	<form> 
              <center>
	               <div class="">
                    <a href="<?php echo base_url(); ?>" class="logo">
                        <img src="<?php echo base_url(); ?>assets/images/logo-wide.png" alt="logo" class="logo-lg"/>
                    </a>
                  </div>
              </center> 
	            <br>
	            <input class="form-control" type="text" required="" placeholder="Username" name="username" value="<?php echo set_value('username'); ?>"/>
	            <br>
	            <input class="form-control" type="password" required="" placeholder="Password" name="password" value="<?php echo set_value('password'); ?>"/> 
	            <br>
	            <button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit" value="Login">Masuk</button> 
	            </form>
              <br>
              <a onclick="add()" class="btn btn-default btn-block">Registrasi</a>
              </div>
	            <br>	                       
	        </div>
          </div>
        </div>
        <div class="col-lg-4">         
        </div>
      </div>
	   </div>
  </div>
</section>
<!--Service-->           
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
       <!--  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> -->
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-scrolltofixed.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.nav.js"></script> 
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.isotope.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/wow.js"></script> 
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom.js"></script>

		  <script src="<?php echo base_url('assets/jquery/jquery-3.1.0.min.js')?>"></script>
		  <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
		  <script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
		  <script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>

        <!-- //// modal -->        
<script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
    var save_method; //for save method string
    var table;
 
     
    function add()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }
 
    
 
    function save()
    {
      var url;

        var nama1 = document.frmOnline.txtnama; 

        var email1 = document.frmOnline.txtemail; 

        var user1 = document.frmOnline.txtuser;

        var pass1 = document.frmOnline.txtpass;

        if (nama1.value == "") {
            alert("Nama Tidak Boleh Kosong");
            txtnama.focus();
            return false;
        }

        if (email1.value == "") {
            alert("Email Tidak Boleh Kosong");
            txtemail.focus();
            return false;
        }
        if (user1.value == "") {
            alert("Username Tidak Boleh Kosong");
            txtuser.focus();
            return false;
        }
        if (pass1.value == "") {
            alert("Password Tidak Boleh Kosong");
            txtpass.focus();
            return false;
        }

      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('login/save')?>";
      }
      else
      {
        url = "<?php echo site_url('')?>";
      }
 
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               $('#modal_form').modal('hide');
              location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Data berhasil disimpan, silahkan masuk');
              $('#modal_form').modal('hide');
              // location.reload();// for reload a page
            }
        });
    }
 
    
 
  </script>
 
  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <h4 class="modal-title">Silahkan Registrasi</h4>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal" name="frmOnline">
          <div class="row">
          <div class="col-lg-10 col-md-offset-1">  
            <!-- //nik,nama,jk,no_telp,pekerjaan,email,username,`passwornd` -->
          <input type="hidden" value="" name="id"/>

          <div class="form-group ">
                    <div class="col-xs-4">
                        <h5>Nama Lengkap</h5>
                    </div>
                    <div class="col-xs-8">
                    <input class="form-control" type="text" name="nama" placeholder="Nama Lengkap" id="txtnama">                  
                  </div>
              </div>  

              <div class="form-group ">
                    <div class="col-xs-4">
                        <h5>Jenis Kelamin</h5>
                    </div>
                    <div class="col-xs-8">
                    <select class="form-control" name="jk" id="txtjk">
                            <option value="-">-</option>
                            <option value="Laki-laki">Laki-laki</option>
                            <option value="Perempuan">Perempuan</option>                            
                    </select>  
                  </div>
              </div> 

              <div class="form-group ">
                    <div class="col-xs-4">
                        <h5>No. Telepon</h5>
                    </div>
                    <div class="col-xs-8">
                    <input class="form-control" type="text" placeholder="No. Telepon" name="no_telp" id="txtno_telp">                 
                  </div>
              </div> 

              <div class="form-group ">
                    <div class="col-xs-4">
                        <h5>Pekerjaan</h5>
                    </div>
                    <div class="col-xs-8">
                    <input class="form-control" type="text" placeholder="Pekerjaan" name="pekerjaan" id="txtpekerjaan">                 
                  </div>
              </div> 

              
              <div class="form-group ">
                    <div class="col-xs-4">
                        <h5>E-Mail</h5>
                    </div>
                    <div class="col-xs-8">
                    <input class="form-control" type="email" name="email" placeholder="E-mail" id="txtemail">                 
                  </div>
              </div>       
                            
               <div class="form-group ">
                    <div class="col-xs-4">
                        <h5>User Name</h5>
                    </div>
                    <div class="col-xs-8">
                    <input class="form-control" type="text" placeholder="User Name" name="username" id="txtuser">                 
                  </div>
              </div>    
               
              <div class="form-group ">
                    <div class="col-xs-4">
                        <h5>Password</h5>
                    </div>
                    <div class="col-xs-8">
                    <input class="form-control" type="password" placeholder="Password" name="password" id="txtpass">
                  </div>
              </div>   
              <div class="modal-footer">              
              <center>
              <button type="button" id="btnSave" onclick="save()" class="btn btn-info waves-effect waves-light">Simpan</button> 
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
              </center>
            </div>
            </div>
            </div>
        </form>
          </div>
          
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->

