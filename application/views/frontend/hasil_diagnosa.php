

    <section class="slider-reviews section-large slider-reviews_1-col bg bg_7 bg_transparent">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
        <h2 class="text-theme-colored text-center">HASIL DIAGNOSA</h2>
        <?php 
          // function get_max_tmp_penyakit(){
          // //Deklarasi function get_max_tmp_penyakit
          //   $results = array();
          //   //deklarasi variable $results
          //   $query = $this->db->query('SELECT kd_penyakit,jml_gejala,cf FROM tmp_penyakit ORDER BY jml_gejala DESC,cf DESC');
          //   //deklarasi dan mengisi variabel $query dengan SELECT kd_penyakit,jml_gejala,cf FROM tmp_penyakit ORDER BY jml_gejala DESC,cf DESC
          //   //yang akan menampilkan penyakit brdasarkan urutan jml_gejala yg dipilih dan nilai cf secara terbesar ke terkecil
          //   return $query->row();
          //   //menjalankan variabel $query
          // }

        echo"<div style='display:none;'>";      
          $get_all_penyakit= $this->Diagnosa_model->get_all_penyakit();
          //memanggil query semua data penyakit
          $no=1; 
          //mengisi variabel $no dengan 1
          foreach ( $get_all_penyakit as $r) { 
            //memulai perulangan sebanyak jumlah penyakit
            $mB_lama=0; $mD_lama=0; $mB_baru=0; $mD_baru=0; $mB_smtra=0; $mD_smtra=0; $cf=0;
            // mengisi variabel Mb dan Md dengan nilai 0
            echo $no.'. '; echo $r->nama_penyakit;echo "<br>";            
            //menampilkan nama penyakit
            $jml_gejala= $this->Diagnosa_model->get_jml_gejala($r->kd_penyakit,$gejala);
            //memanggil query yg menghitung jumlah gejala yg dipilih
            $rule= $this->Diagnosa_model->get_rule($r->kd_penyakit,$gejala);
            //memanggil query tabel rule berdasarkan semua penyakit dan gejala yg dipilih
            foreach ($rule as $row) {
            //memulai perulangan sebanyak rule pada penyakit hasil query d atas
             echo ' - '; echo $row->gejala.' >> MB: '; echo $row->mb.' MD: '; echo $row->md;echo "<br>";
             //menampilkan gejala, nilai Mb dan Md dari penyakit d atas
             echo "MB Lama ".$mB_lama=$mB_smtra; echo "<br>";
             //menampilkan variabel $mB_lama yg sebelumnya diisi dengan $mB_smtra
             echo "MD Lama ".$mD_lama=$mD_smtra; echo "<br>";
             //menampilkan variabel $mD_lama yang sebelumnya diisi dengan $mD_smtra
             echo "MB Baru ".$mB_baru=$row->mb; echo "<br>";
             //menampilkan variabel $mB_baru yang sebelumnya diisi dengan Mb dari rule di atas
             echo "MD Baru ".$mD_baru=$row->md; echo "<br>";
             //menampilkan variabel $mD_baru yang sebelumnya diisi dengan Md dari rule di atas
             echo "MB Sementara ".$mB_smtra=$mB_lama+($mB_baru*(1-$mB_lama)); echo "<br>";
             //menampilkan variabel mB_smtra yang sebelumnya diisi dengan $mB_lama+($mB_baru*(1-$mB_lama)
             echo "MD Sementara ".$mD_smtra=$mD_lama+($mD_baru*(1-$mD_lama)); echo "<br>";          
             //menampilkan variabel mD_smtra yang sebelumnya diisi dengan $mD_lama+($mD_baru*(1-$mD_lama)
            }
            echo "CF: ".$cf=$mB_smtra - $mD_smtra;echo "<br>";
            //menampilkan variabel $cf yang sebelumnya diisi dengan $mB_smtra - $mD_smtra
            $dataCF = array(
            //deklarasi variabel array $dataCF
                              'kd_penyakit' => $r->kd_penyakit,
                              //mengisi array kd_penyakit dengan variabel $r->kd_penyakit 
                              'cf' => $cf,
                              //mengisi array cf dengan variabel $cf 
                              'jml_gejala' => $jml_gejala->jml_gejala,
                              //mengisi array jml_gejala dengan variabel $jml_gejala 
                      );
            $insert = $this->Diagnosa_model->tmp_penyakit_add($dataCF);
            //memanggil query untuk menyimpan variabel array $dataCF
          $no++; }
          //menambah 1 variabel $no
          $max_tmp_penyakit = $this->Diagnosa_model->get_max_tmp_penyakit();
          //memanggil query untuk mencari nilai CF maximal dari smua penyakit
         
          $data = array(   
          //deklarasi variabel array $data               
              'id_pasien' => $this->session->userdata('id'),
              //mengisi array id_pasien dengan id user yang melakukan diagnosa
              'kd_penyakit' => $max_tmp_penyakit->kd_penyakit,
              //mengisi array kd_penyakit dengan penyakit terpilih
              'cf' => $max_tmp_penyakit->cf,
              //mengisi array cf dengan variabel cf tertinggi
              'tanggal' => date('Y-m-d'),               
              //mengisi array tanggal hari ini
              'gejala' => implode(",", $this->input->post("gejala")),               
              //mengisi array gejala yang dipilih
              'waktu' => date('Y-m-d h:i:s'),                    
              //mengisi array waktu diagnosa
              );
          $insert = $this->Diagnosa_model->hasil_add($data);
          //memanggil query untuk menyimpan variabel array $data
         echo "</div>"; 
         ?>

        <h4 class="block__title"><strong>Nama Pasien : <?php echo $this->session->userdata('nama'); ?></strong></h4>                                             
          <h4 class="block__title"><strong>Gejala yang dipilih :</strong></h4>    
        <table id="tbl-list" class="table table-bordered table-striped">
          <tr>
            <th width="50px">No</th>
            <th>Gejala</th>
          </tr>
          <tr>
            <?php $i = 1; foreach($listGejala as $value){
              $cek_rule= $this->Diagnosa_model->get_row_rule($max_tmp_penyakit->kd_penyakit,$value->id_gejala)->num_rows(); 
              $rule= $this->Diagnosa_model->get_row_rule($max_tmp_penyakit->kd_penyakit,$value->id_gejala)->row(); { 
              if ($cek_rule !== 0) $mb=$rule->mb; else $mb='-';
              if ($cek_rule !== 0) $md=$rule->md; else $md='-'; ?>
              <tr>
                <td width="30px"><?php echo $i++?></td>
                  <td><?php echo $value->gejala?> <br><?php echo ' MB: '.$mb.' MD: '.$md; ?></td>
                </tr>
            <?php }?>
            <?php }?>
          </tr>
        </table>  
        <!-- <h4 class="block__title"><strong>NIK : <?php echo $this->session->userdata('nik'); ?> </strong></h4>                     -->
        <h4 class="block__title"><strong>Pasien Menderita :  </strong></h4>  <!-- <?php echo $penyakit_up->nama_penyakit; ?> -->
        <table id="tbl-list" class="table table-bordered table-striped">
          <tr>
            <th>Nama Penyakit</th>
            <th>CF</th>
            <th>Defenisi</th>
            <th>Solusi</th>
          </tr>
          
            <?php $i = 1; 
            $up = $this->Diagnosa_model->get_max_tmp_penyakit();
            $daftar_penyakit = $this->Diagnosa_model->get_max_penyakit_tmp_penyakit($up->jml_gejala,$up->cf);
            foreach($daftar_penyakit as $r){
                $penyakit_up = $this->Diagnosa_model->get_penyakit_where($r->kd_penyakit);
                //memanggil query untuk mencari penyakit terpilih
                $solusi = $this->Diagnosa_model->get_solusi($penyakit_up->id_penyakit);
                //memanggil query untuk menampilkan solusi dari penyakit terpilih
                { ?>
                <tr>
                  <td><?php echo $penyakit_up->nama_penyakit; ?></td>
                  <td><?php echo $r->cf; ?></td>
                  <td><?php echo $solusi->defenisi; ?></td>
                  <td><?php echo $solusi->solusi; ?></td>
              
                </tr>
            <?php }
            } ?>
        </table>

        <!-- <div class="features-details">
            <h4>Pasien Menderita : <?php echo $penyakit_up->nama_penyakit; ?> dengan nilai CF = <?php echo $max_tmp_penyakit->cf; ?></h4>
            <h4>Defenisi Penyakit :</h4>
            <p><?php echo $solusi->defenisi; ?></p>
        </div>
        <div class="features-details">
            <h4>Solusi :</h4>
            <p><?php echo $solusi->solusi; ?></p>
        </div> -->

      <div class="col-sm-4">
                <a href="<?php echo base_url(); ?>diagnosa" class="btn btn-flat btn-sm btn-theme-colored mt-15  text-theme-color-2">Kembali</a>
                <a href="<?php echo base_url(); ?>riwayat" class="btn btn-flat btn-sm btn-theme-colored mt-15  text-theme-color-2">Riwayat</a>
                <!-- <a href="<?php echo base_url(); ?>rekomendasi/cetak/<?php echo $ujitanah_DSC->id ?>"><button class="theme_btn">Cetak</button></a> -->
                
              </div>
      </div>
    </div>
  </section>

 