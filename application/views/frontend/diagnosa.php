<?php echo form_open('diagnosa'); ?>
<section id="services" class="divider parallax layer-overlay overlay-theme-colored-9">
    	<div class="container pb-50">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="text-uppercase text-white mt-0 line-height-1">Pilih Gejala yang dialami</h2>
            </div>
          </div>
        </div>
    		<div class="section-content">
    			<div class="row">
    				<div class="col-sm-12">
    					<?php foreach($gejala as $gejala){?>
                        <div class="form-group" style="margin-left:50px">
                            <label class="text-white">
                            <input type="checkbox" name="gejala[]" value="<?php echo $gejala->id_gejala?>" /> <?php echo $gejala->gejala?>
                            </label>
                        </div>                    
                <?php }?>
                <div>
                    <center>
                      <div>
                        <input class="btn btn-dark btn-theme-colored" data-loading-text="Please wait..." type="submit" value="Proses" />
                      </div>
                    </center>
                </div>   
            </div>
    			</div>
    		</div>
    	</div>
    </section>
</form>