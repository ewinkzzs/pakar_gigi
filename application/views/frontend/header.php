<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>

<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="Sistem Pakar Penyakit Gigi dan Mulut" />
<meta name="keywords" content="keyword1,keyword2,keyword3,keyword4,keyword5" />
<meta name="author" content="ThemeMascot" />

<!-- Page Title -->
<title>SIPAGIMUT</title>

<!-- Favicon and Touch Icons -->
<link href="<?php echo base_url(); ?>assets/images/logo.png" rel="shortcut icon" type="image/png">
<link href="<?php echo base_url(); ?>assets/images/apple-touch-icon.png" rel="apple-touch-icon">
<link href="<?php echo base_url(); ?>assets/images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
<link href="<?php echo base_url(); ?>assets/images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
<link href="<?php echo base_url(); ?>assets/images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link href="<?php echo base_url(); ?>assets/css/menuzord-megamenu.css" rel="stylesheet"/>
<link id="menuzord-menu-skins" href="<?php echo base_url(); ?>assets/css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="<?php echo base_url(); ?>assets/css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="<?php echo base_url(); ?>assets/css/preloader.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="<?php echo base_url(); ?>assets/css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- Revolution Slider 5.x CSS settings -->
<link  href="<?php echo base_url(); ?>assets/js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link  href="<?php echo base_url(); ?>assets/js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
<link  href="<?php echo base_url(); ?>assets/js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>

<!-- CSS | Theme Color -->
<link href="<?php echo base_url(); ?>assets/css/colors/theme-skin-color-set1.css" rel="stylesheet" type="text/css">

<!-- external javascripts -->
<script src="<?php echo base_url(); ?>assets/js/jquery-2.2.4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="<?php echo base_url(); ?>assets/js/jquery-plugin-collection.js"></script>

<!-- Revolution Slider 5.x SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
	<script src="<?php echo base_url(); ?>assets/https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="">
<div id="wrapper">
	<!-- preloader -->
	<!-- <div id="preloader">
		<div id="spinner">
			<div class="preloader-dot-loading">
				<div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
			</div>
		</div>
		<div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
	</div> -->
  
  <!-- Header -->
  <header id="header" class="header ">
    <!-- <div class="header-top sm-text-center">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="widget text-white">
              <i class="fa fa-clock-o text-theme-colored"></i> Jam Buka:  Senin - Kamis, Sabtu : 17.00 PM - 22.00 PM, Jumat dan Minggu Tutup
            </div>
          </div>
          <div class="col-md-6">
            <div class="widget">
              <ul class="list-inline pull-right flip sm-pull-none sm-text-center bordered-list">
                <li class="text-white"><i class="fa fa-phone text-theme-colored"></i> Call Us at <a class="text-white" href="#">(0411) 432 634</a></li>
                <li><i class="fa fa-map-marker text-theme-colored"></i> <a class="text-white" href="#">Jl. Abd. Dg. Siruah No. 66, Makassar</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    <div class="header-nav navbar-scrolltofixed navbar-sticky-animated">
      <div class="header-nav-wrapper">
        <div class="container">
          <nav id="menuzord-right" class="menuzord blue bg-white">
            <a class="menuzord-brand pull-left flip mb-15" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/logo-wide.png" alt=""></a>
            <ul class="menuzord-menu">
              <li class="<?php if($this->uri->segment(1)==""){echo "active";}?>"><a href="<?php echo base_url(); ?>">Beranda</a></li>
              <?php if ($this->session->userdata('username') != '') { ?> 
              <li class="<?php if($this->uri->segment(1)=="diagnosa"){echo "active";}?>"><a href="<?php echo base_url(); ?>diagnosa">Diagnosa</a></li>
              <li class="<?php if($this->uri->segment(1)=="riwayat"){echo "active";}?>"><a href="<?php echo base_url(); ?>riwayat">Riwayat</a></li>
              <?php } ?>
              <li class="<?php if($this->uri->segment(1)=="petunjuk"){echo "active";}?>"><a href="<?php echo base_url(); ?>petunjuk">Petunjuk</a></li>
              <?php if ($this->session->userdata('username') != '') { ?> 
              <li class="<?php if($this->uri->segment(1)=="login"){echo "active";}?>"><a href="<?php echo base_url(); ?>login/logout">Keluar</a></li>
              <?php } else { ?>
              <li class="<?php if($this->uri->segment(1)=="login"){echo "active";}?>"><a href="<?php echo base_url(); ?>login">Masuk</a></li>
              <?php } ?>

            </ul>
          </nav>
        </div>
      </div>
    </div>
  </header>