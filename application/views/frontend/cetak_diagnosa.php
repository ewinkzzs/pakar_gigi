<!DOCTYPE html>
<html><head>
<link rel="shortcut icon" href="<?=base_url('assets/frontend/images/X.ico');?>">
  
<title>Hasil Diagnosa ></title>
  
	<style>
	  
		table{
		      
		border-collapse: collapse;
		      
		/*width: 100%;*/
		      
		/*margin: 0 auto;*/
		  
		}
		  
		table th{
		      
		border:1px solid #000;
		      
		padding: 3px;
		      
		font-weight: bold;
		      
		text-align: center;
		  
		}
		  
		table td{
		      
		border:1px solid #000;
		      
		padding: 3px;
		      
		vertical-align: top;
		  
		}
	</style>
</head><body>
<!-- kop -->
<h2 style="text-align: center; font-weight: bold;">Apotek Anwad Farma</h2>
<p style="text-align: center;font-weight: bold;">Jl. Abd. Daeng Sirua No. 66 </p>
<p style="text-align: center;">Buka:  Senin - Kamis, Sabtu : 17.00 PM - 22.00 PM, Jumat dan Minggu Tutup</p>
<hr>
<!-- N Kop -->

<h3 style="text-align: center; font-weight: bold;">Hasil Diagnosa</h3>
<!-- <p style="font-weight: bold;">NIK : <?php echo $this->session->userdata('nik'); ?></p> -->
<p style="font-weight: bold;">Nama Pasien : <?php echo $this->session->userdata('nama'); ?></p>
<p style="font-weight: bold;">Waktu Diagnosa : <?php echo $row->waktu;?></p>
<p style="font-weight: bold">Gejala yang dipilih :</p>
	<?php $i = 1; foreach($listGejala as $value){
		$cek_rule= $this->Diagnosa_model->get_row_rule($nm_penyakit->kd_penyakit,$value->id_gejala)->num_rows(); 
        $rule= $this->Diagnosa_model->get_row_rule($nm_penyakit->kd_penyakit,$value->id_gejala)->row(); { 
        	if ($cek_rule !== 0) $mb=$rule->mb; else $mb='-';
            if ($cek_rule !== 0) $md=$rule->md; else $md='-'; ?>
        <?php echo $i++."."?>
        <?php echo $value->gejala?><br>&nbsp;&nbsp;&nbsp;<?php echo ' MB: '.$mb.' MD: '.$md; ?><br>
    <?php }?>
    <?php }?>

<br>
<p style="font-weight: bold">Pasien Menderita : </p>
<!-- // <?php echo $nm_penyakit->nama_penyakit; ?> dengan nilai CF = <?php echo $row->cf; ?>-->
	<table>    
		<tr>
			<td>Nama Penyakit</td>
            <td>CF</td>
            <td>Defenisi</td>
            <td>Solusi</td>
         </tr>          
            <?php  
            $up = $this->Diagnosa_model->get_max_tmp_penyakit();
            $daftar_penyakit = $this->Diagnosa_model->get_max_penyakit_tmp_penyakit($up->jml_gejala,$up->cf);
            foreach($daftar_penyakit as $r){ { ?>
                <?php 
                $penyakit_up = $this->Diagnosa_model->get_penyakit_where($r->kd_penyakit);
                $solusi = $this->Diagnosa_model->get_solusi($penyakit_up->id_penyakit);  ?>
                
                <tr>
                  <td><?php echo $penyakit_up->nama_penyakit; ?></td>
                  <td><?php echo $r->cf; ?></td>
                  <td><?php echo $solusi->defenisi; ?></td>
                  <td><?php echo $solusi->solusi; ?></td>
              
                </tr>
            <?php }
            } ?>
    </table>
 <!-- <p style="font-weight: bold">Defenisi Penyakit :</p>   
 <p><?php echo $solusi->defenisi;?></p>
 <p style="font-weight: bold">Solusi Penyakit :</p>   
 <p><?php echo $solusi->solusi;?></p> -->
<br>
<!-- //	 -->
</body></html>