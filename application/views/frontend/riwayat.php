<section id="about">
      <div class="container pt-0">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="text-uppercase text-black mt-0 line-height-1">Diwayat Diagnosa</h2>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-sm-12">
            <table id="tbl-list" class="table table-bordered table-striped">
               <thead>
                        <tr>
                            <td widtd="3%">No</td>
                            <td widtd="10%">Waktu Diagnosa</td>
                            <td widtd="20%">Hasil Diagnosa</td>     
                            <td widtd="10%">Aksi</td>
                        </tr>
                    </thead>                
                    <tbody>
                    <?php $nomor=1; 
                    $this->load->model('Hasil_model');
                    $hasil = $this->Hasil_model->get_by_id($this->session->userdata('id'));
                    
                      foreach($hasil as $hasil) {?>
                        <tr>                                                
                            <td><?php echo $nomor;?></td>                                               
                            <td><?php echo $hasil->waktu;?></td>
                            <td><?php echo $hasil->nama_penyakit;?></td>
                            <td><a class="btn btn-dark btn-theme-colored" href="<?php echo base_url(); ?>riwayat/cetak/<?php echo $hasil->id;?>">Cetak</a>
                              <!-- <a class="btn btn-flat btn-sm btn-theme-colored mt-15  text-theme-color-2" href="hasil_diagnosa/view_diagnosa/<?php echo $hasil->id;?>"> Cetak </a> -->
                            </td> 
                        </tr>   
                      <?php $nomor++;}?>                     
                  
                    </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
</section>