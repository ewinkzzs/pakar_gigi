<section class="slider-reviews section-large slider-reviews_1-col bg bg_7 bg_transparent">
	<div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="text-uppercase mt-0 line-height-1">Petunjuk</h2>
            </div>
          </div>
        </div>
    
      <div class="row">
        <div class="col-xs-12">
        	<br><br>
        	   <p>Jika Anda belum registrasi, maka silahkan registrasi dengan mengklik menu masuk kemudian "klik registrasi".</p>
        	<br>
             <center><img src="<?php echo base_url(); ?>assets/images/petunjuk/1.jpg"></center>               
             <br><br>
        	   <p>Setelah muncul form registrasi, silahkan isi data registrasi lalu klik tombol "Simpan".</p>
        	   <br>
             <center><img src="<?php echo base_url(); ?>assets/images/petunjuk/2.jpg"></center>                <br><br>
        	   <p>Setelah registrasi, silahkan klik tulisan "Masuk" yang berada di Header lalu masukkan Username dan Password kemudian klik tombol "Masuk".</p>
        	   <br>
             <center><img src="<?php echo base_url(); ?>assets/images/petunjuk/3.jpg"></center>
             <br><br>
        	   <p>Setelah masuk, silahkan klik tulisan "Diagnosa" untuk memulai diagnosa.</p>
        	   <br>
             <center><img src="<?php echo base_url(); ?>assets/images/petunjuk/4.jpg"></center>
             <br><br>
        	   <p>Setelah muncul tampilan diagnosa, silahkan pilih gejala yang anda alami lalu klik tombol "Proses".</p>
        	   <br>
             <center><img src="<?php echo base_url(); ?>assets/images/petunjuk/5.jpg"></center>    
             	<br><br>
             	<p>Setelah mengklik tombol proses, maka akan muncul hasil diagnose seperti Diatas.</p>
             <br>
             <center><img src="<?php echo base_url(); ?>assets/images/petunjuk/6.jpg"></center>    
             	<br><br>
             	<p>Untuk mencetak hasil diagnosa, silahkan klik option yang ada dalam lingkar merah kemudian pilih riwayat.</p>
             <br>
             <center><img src="<?php echo base_url(); ?>assets/images/petunjuk/7.jpg"></center>    
             	<br><br>
             	<p>Lalu klink cetak, maka akan secara otomatis terdownload. Dan hasil cetakan seperti gambar diatas.</p>
             <br>
         
         
        </div>
      </div>
    </div>
  </section>