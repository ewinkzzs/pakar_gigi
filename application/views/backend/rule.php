
            

                <div class="page-inner">
                    
                    <div id="main-wrapper">
                        <div class="row">
                            <div class="col-md-12">                            
                                <div class="panel panel-white">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Data Rule</h4>
                                        <button type="button" class="btn btn-success m-b-sm" onclick="add_()" data-toggle="modal" data-target="#con-close-modal"><i class="glyphicon glyphicon-plus"></i> Tambah</button>                                   
                                    </div>
                                    <div class="panel-body">
                                        
                                        <!-- Modal -->
                                        
                                        <div class="table-responsive">
                                            <table id="example3" class="display table" style="width: 100%; cellspacing: 0;">
                                                <thead>
                                                    <tr>
                                                        <th style="width:20px;"><b>No</b></th>
                                                        <th><b>Penyakit dan Gejalanya</b></th>
                                                        <th><b>MB</b></th>                
                                                        <th><b>MD</b></th>                
                                                        <th style="width:10px;"><b>Aksi</b></th>
                                                    </tr>
                                                </thead>
                                                
                                                <tbody>
                                                    <?php $no=1; foreach ($penyakit as $r) { ?>
                                                      <tr>
                                                          <td><?php echo $no;?></td>                    
                                                          <td><?php echo $r->nama_penyakit;?></td>                              
                                                          <td></td>   
                                                          <td></td>   
                                                      </tr>
                                                      <?php 
                                                        $rule = $this->Rule_model->get_where($r->kd_penyakit);
                                                        foreach($rule as $rule) { ?> 
                                                        <tr> 
                                                          <td></td>
                                                          <td class="text-primary"><?php echo $rule->gejala;?></td>
                                                          <td class="text-primary"><?php echo $rule->mb;?></td>
                                                          <td class="text-primary"><?php echo $rule->md;?></td>            
                                                          <td>   
                                                          <button class="btn btn-success" onclick="edit_(<?php echo $rule->id;?>)"><i class="glyphicon glyphicon-pencil"></i></button>                    
                                                          <button class="btn btn-danger"  onclick="delete_(<?php echo $rule->id;?>)"><i class="glyphicon glyphicon-remove"></i></button>
                                                        </td>
                                                        </tr> 
                                                        <?php } ?>
                                                    <?php $no++; } ?>

                                                    
                                                </tbody>                                        
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Row -->
                    </div><!-- Main Wrapper -->
                    <div class="page-footer">
                        <p>© Copyright by STMIK HANDAYANI</p>
                    </div>
                </div><!-- /Page Inner -->
                
            </div><!-- /Page Content -->
        </div><!-- /Page Container -->
        
        
        <!-- Javascripts -->
        <script src="<?php echo base_url(); ?>assets_admin/plugins/jquery/jquery-3.1.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/uniform/js/jquery.uniform.standalone.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/switchery/switchery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/datatables/js/jquery.datatables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/js/space.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/js/pages/table-data.js"></script>
    </body>
</html>

<script type="text/javascript">
        
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
    var save_method; //for save method string
    var table;
 
     
    function add_()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }

    function edit_(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('admin/rule_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="kd_gejala"]').val(data.kd_gejala);
            $('[name="kd_penyakit"]').val(data.kd_penyakit);
            $('[name="md"]').val(data.md);
            $('[name="mb"]').val(data.mb);

            $('#con-close-modal').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Ubah Data Rule'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }
  
    function save()
    {
      var url;
        // <!-- ,dosis,aturan_pakai,ket -->
        var kd_gejala = document.frmOnline.kd_gejala; 

        var kd_penyakit = document.frmOnline.kd_penyakit;
        var md = document.frmOnline.md;
        var mb = document.frmOnline.mb;


        if (kd_gejala.value == "NULL") {
            alert("Gejala Tidak Boleh Kosong");
            kd_gejala.focus();
            return false;
        }
        if (kd_penyakit.value == "NULL") {
            alert("Penyakit Tidak Boleh Kosong");
            kd_penyakit.focus();
            return false;
        }
        if (md.value == "") {
            alert("MD Tidak Boleh Kosong");
            md.focus();
            return false;
        }
        if (mb.value == "") {
            alert("MB Tidak Boleh Kosong");
            mb.focus();
            return false;
        }
      /////
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('admin/rule_add')?>";
      }
      else
      {
        url = "<?php echo site_url('admin/rule_update')?>";
      }
 
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               alert('Data berhasil disimpan');
               $('#modal_form').modal('hide');
              location.reload();// for reload a page
              
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_(id)
    {
      if(confirm('Anda Yakin ingin menghapus data ini ?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('admin/rule_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

      }
    }
 
  </script>                                    

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 class="modal-title">Masukkan Data Rule</h4> 
            </div> 
          <form class="form-appointment ui-form" action="#" id="form" onsubmit="return Validation()" name="frmOnline">
          <div class="row">
            <div class="col-lg-10 col-md-offset-1">    
              <br>    
              <input type="hidden" value="" name="id"/>   
              <div class="form-group ">
                  <div class="col-xs-12">
                    <label for="form_control_1">Pilih Penyakit</label>
                    <select class="form-control" name="kd_penyakit" id="kd_penyakit">
                            <option value="NULL">Pilih Penyakit</option>
                            <?php 
                            $penyakit=$this->Penyakit_model->get_all();
                            foreach($penyakit as $penyakit) {?>
                              <option value="<?php echo $penyakit->kd_penyakit;?>"><?php echo $penyakit->nama_penyakit;?></option>
                            <?php } ?>
                        </select>  
                  </div>
              </div>  
              <br> <br> <br>     
              <div class="form-group ">
                   <!-- ,dosis,aturan_pakai,ket -->
                  <div class="col-xs-12">
                    <label for="form_control_1">Pilih Gejala</label>
                      <select class="form-control" name="kd_gejala" id="kd_gejala">
                            <option value="NULL">Pilih Gejala</option>
                            <?php 
                            $gejala=$this->Gejala_model->get_all();
                            foreach($gejala as $gejala) {?>
                              <option value="<?php echo $gejala->id_gejala;?>"><?php echo $gejala->gejala;?></option>
                            <?php } ?>
                        </select>  
                       
                  </div> 
              </div>  
              <br> <br> <br>                  
              <div class="form-group ">
                  <div class="col-xs-6">
                    <label for="form_control_1">Nilai MB</label>
                      <input class="form-control" type="text" name="mb" placeholder="Nilai MB" id="mb">
                  </div>
                 
                  <div class="col-xs-6">
                    <label for="form_control_1">Nilai MD</label>
                      <input class="form-control" type="text" name="md" placeholder="Nilai MD" id="md">     
                  </div>
              </div>       
                           
                             
              <br> <br>                      
            </div>
          </div><!-- end row -->
            <div class="modal-footer">               
              <button type="button" id="btnSave" onclick="save()" class="btn btn-info waves-effect waves-light">Simpan</button> 
            </div> 
        </div> 
    </div>
</div>                                   