<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Responsive Admin Dashboard Template">
        <meta name="keywords" content="admin,dashboard">
        <meta name="author" content="stacks">
        <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        
        <!-- Title -->
        <title>Space - Responsive Admin Dashboard Template</title>

        <!-- Styles -->
        <link href="<?php echo base_url(); ?>assets/images/logo.png" rel="shortcut icon" type="image/png">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets_admin/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets_admin/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets_admin/plugins/icomoon/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets_admin/plugins/uniform/css/default.css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>assets_admin/plugins/switchery/switchery.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>assets_admin/plugins/datatables/css/jquery.datatables.min.css" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url(); ?>assets_admin/plugins/datatables/css/jquery.datatables_themeroller.css" rel="stylesheet" type="text/css"/>	
        <link href="<?php echo base_url(); ?>assets_admin/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css"/>
      
        <!-- Theme Styles -->
        <link href="<?php echo base_url(); ?>assets_admin/css/space.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets_admin/css/custom.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        
        <!-- Page Container -->
        <div class="page-container">
            <!-- Page Sidebar -->
            <div class="page-sidebar">
                <a class="logo-box" href="<?php echo base_url(); ?>admin">
                    <!-- <img src="<?php echo base_url(); ?>assets/images/logo-wide.png" alt=""> -->
                    <i class="icon-close" id="sidebar-toggle-button-close"></i>
                </a>
                <div class="page-sidebar-inner">
                    <div class="page-sidebar-menu">
                        <ul class="accordion-menu">
                            <li class="<?php if($this->uri->segment(2)==""){echo "active-page";}?>">
                                <a href="<?php echo base_url(); ?>admin">
                                    <i class="menu-icon icon-home4"></i><span>Dashboard</span>
                                </a>
                            </li>
                            <li class="<?php if($this->uri->segment(2)=="penyakit"){echo "active-page";}?>">
                                <a href="<?php echo base_url(); ?>admin/penyakit">
                                    <i class="menu-icon icon-inbox"></i><span>Penyakit</span>
                                </a>
                            </li>
                            <li class="<?php if($this->uri->segment(2)=="gejala"){echo "active-page";}?>">
                                <a href="<?php echo base_url(); ?>admin/gejala">
                                    <i class="menu-icon icon-inbox"></i><span>Gejala</span>
                                </a>
                            </li>
                            <li class="<?php if($this->uri->segment(2)=="solusi"){echo "active-page";}?>">
                                <a href="<?php echo base_url(); ?>admin/solusi">
                                    <i class="menu-icon icon-inbox"></i><span>Solusi</span>
                                </a>
                            </li>
                            <li class="<?php if($this->uri->segment(2)=="rule"){echo "active-page";}?>">
                                <a href="<?php echo base_url(); ?>admin/rule">
                                    <i class="menu-icon icon-inbox"></i><span>Rule</span>
                                </a>
                            </li>
                            <li class="<?php if($this->uri->segment(2)=="user"){echo "active-page";}?>">
                                <a href="<?php echo base_url(); ?>admin/user">
                                    <i class="menu-icon icon-inbox"></i><span>Pasien</span>
                                </a>
                            </li>
                            <li class="<?php if($this->uri->segment(2)=="hasil"){echo "active-page";}?>">
                                <a href="<?php echo base_url(); ?>admin/hasil">
                                    <i class="menu-icon icon-inbox"></i><span>Hasil Diagnosa</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>login/logout">
                                    <i class="menu-icon icon-help_outline"></i><span>Keluar</span>
                                </a>
                            </li>                            
                        </ul>
                    </div>
                </div>
            </div><!-- /Page Sidebar -->
                <!-- Page Content -->
            <div class="page-content">            
                <!-- Page Header -->
                <div class="page-header">
                
                    <nav class="navbar navbar-scrolltofixed">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <div class="logo-sm">
                                    <a href="javascript:void(0)" id="sidebar-toggle-button"><i class="fa fa-bars"></i></a>
                                    <img src="<?php echo base_url(); ?>assets/images/logo-wide.png" width="230" height="28" alt="">
                                </div>
                               
                            </div>
                        
                            <!-- Collect the nav links, forms, and other content for toggling -->
                        
                            
                        </div><!-- /.container-fluid -->
                    </nav>
                </div><!-- /Page Header -->
                <!-- Page Inner -->
         
