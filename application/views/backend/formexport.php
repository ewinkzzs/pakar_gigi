
                <!-- Page Inner -->
                <div class="page-inner">
                    
                    <div id="main-wrapper">
                        <div class="row">
                            <div class="col-md-12">   
                            	<div class="panel-heading">
                            		<center>
                                	<h3 class="panel-title">Export Data Diagnosa</h3>    
                                	</center>
                             	</div>
	                            <form class="form-appointment ui-form" method="POST" action="<?php echo base_url(); ?>admin/export" name="frmOnline"> <!-- target="_blank" -->
						          <div class="row">
						            <div class="col-lg-10 col-md-offset-1">           
						              <input type="hidden" value="" name="id_penyakit"/>
						              <div class="form-group ">
						                   <!-- ,dosis,aturan_pakai,ket -->
						                  <div class="col-xs-12">
						                    <label for="form_control_1">Tanggal Mulai</label>
						                    <input class="form-control" type="date" name="tgl_1">     
						                    </div>
						              </div>  
						              <br> <br>                  
						              <br> <br>                 
						              <div class="form-group ">
						                  <div class="col-xs-12">
						                    <label for="form_control_1">Sampai Tanggal</label>
						                    <input class="form-control" type="date" name="tgl_2">
						                    </div>
						              </div>       
						              <br> <br>                      

						            <div class="modal-footer">               
						              <button type="submit" target="_blank" class="btn btn-success">Export</button> 
						            </div> 
						            </div> 

						        </div> 
						        </form>                     
                                <button><a href="<?php echo base_url(); ?>admin/export">Export</a></button>
                           
                    		</div>
                    </div><!-- Main Wrapper -->
                    <div class="page-footer">
                        <p>© Copyright by STMIK HANDAYANI</p>
                    </div>
                </div><!-- /Page Inner -->
                
            </div><!-- /Page Content -->
        </div><!-- /Page Container -->
    </body>
</html>                                  
<!-- Javascripts -->
        <script src="<?php echo base_url(); ?>assets_admin/plugins/jquery/jquery-3.1.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/uniform/js/jquery.uniform.standalone.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/switchery/switchery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/datatables/js/jquery.datatables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/js/space.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/js/pages/table-data.js"></script>