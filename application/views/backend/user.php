
            

                <div class="page-inner">
                    
                    <div id="main-wrapper">
                        <div class="row">
                            <div class="col-md-12">                            
                                <div class="panel panel-white">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Data Pasien</h4>
                                        <button type="button" class="btn btn-success m-b-sm" onclick="add_()" data-toggle="modal" data-target="#con-close-modal"><i class="glyphicon glyphicon-plus"></i> Tambah</button>                                   
                                    </div>
                                    <div class="panel-body">
                                        
                                        <!-- Modal -->
                                        
                                        <div class="table-responsive">
                                            <table id="example3" class="display table" style="width: 100%; cellspacing: 0;">
                                                <thead>
                                                    <tr>
                                                        <th style="width:50px;"><b>No</b></th>
                                                        <th><b>Nama</b></th>
                                                        <th><b>Email</b></th>
                                                        <th><b>Username</b></th>       
                                                        <th style="width:125px;"><b>Aksi</b></th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th><b>No</b></th>
                                                        <th><b>Nama</b></th>
                                                        <th><b>Email</b></th>
                                                        <th><b>Username</b></th>       
                                                        <th style="width:125px;"><b>Aksi</b></th>
                                                    </tr>
                                                </tfoot>
                                                <tbody>
                                                    <?php $no=1; foreach ($user as $r) { ?>

                                                    <tr>
                                                        <td><?php echo $no;?></td> 
                                                        <td><?php echo $r->nama;?></td>
                                                        <td><?php echo $r->email;?></td>
                                                        <td><?php echo $r->username;?></td>
                                                        <td>
                                                            <!-- <button class="btn btn-success" onclick="edit_(<?php echo $r->id;?>)"><i class="glyphicon glyphicon-pencil"></i></button> -->                                                    
                                                            <button class="btn btn-danger" onclick="delete_(<?php echo $r->id;?>)"><i class="glyphicon glyphicon-remove"></i></button>
                                                        </td>   
                                                    </tr>
                                                    <?php $no++; } ?>
                                                    
                                                </tbody>                                        
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Row -->
                    </div><!-- Main Wrapper -->
                    <div class="page-footer">
                        <p>© Copyright by STMIK HANDAYANI</p>
                    </div>
                </div><!-- /Page Inner -->
                
            </div><!-- /Page Content -->
        </div><!-- /Page Container -->
        
        
        <!-- Javascripts -->
        <script src="<?php echo base_url(); ?>assets_admin/plugins/jquery/jquery-3.1.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/uniform/js/jquery.uniform.standalone.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/switchery/switchery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/datatables/js/jquery.datatables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/js/space.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/js/pages/table-data.js"></script>
    </body>
</html>

<script type="text/javascript">
        
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
    var save_method; //for save method string
    var table;
 
     
   function add_()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }
  
    function save()
    {
      var url;

        var email1 = document.frmOnline.txtemail; 

        var user1 = document.frmOnline.txtuser;

        var pass1 = document.frmOnline.txtpass;


        if (email1.value == "") {
            alert("Email Tidak Boleh Kosong");
            txtemail.focus();
            return false;
        }
        if (user1.value == "") {
            alert("Username Tidak Boleh Kosong");
            txtuser.focus();
            return false;
        }
        if (pass1.value == "") {
            alert("Password Tidak Boleh Kosong");
            txtpass.focus();
            return false;
        }
      /////
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('/admin/user_add')?>";
      }
      else
      {
        url = "<?php echo site_url('/admin/user_update')?>";
      }
 
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               alert('Data berhasil disimpan');
               $('#modal_form').modal('hide');
              location.reload();// for reload a page
              
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('admin/user_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

      }
    }
 
  </script>

  <!-- modal -->
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 class="modal-title">Tambah Data User</h4> 
            </div> 
          <form class="form-appointment ui-form" action="#" id="form" onsubmit="return Validation()" name="frmOnline">
          <div class="row">
            <div class="col-lg-10 col-md-offset-1">    
              <br>          
              <div class="form-group ">
                    <div class="col-xs-12">
                      <label for="form_control_1">Nama Lengkap</label>
                    <input class="form-control" type="text" name="nama" placeholder="Nama Lengkap" id="txtnama">                  
                  </div>
              </div>  
              <br> <br>                  
              <div class="form-group ">
                    <div class="col-xs-12">
                      <label for="form_control_1">E-mail</label>
                      <input class="form-control" type="email" name="email" placeholder="E-mail" id="txtemail">                 
                  </div>
              </div>       
              <br> <br>               
               <div class="form-group ">
                    <div class="col-xs-12">
                    <label for="form_control_1">User Name</label>
                    <input class="form-control" type="text" placeholder="User Name" name="username" id="txtuser">                 
                  </div>
              </div>    
              <br> <br>  
              <div class="form-group ">
                    <div class="col-xs-12">
                    <label for="form_control_1">Password</label>
                    <input class="form-control" type="password" placeholder="Password" name="password" id="txtpass">
                  </div>
              </div>   
              <input class="form-control" type="hidden" name="level" id="txtlevel" value="Pasien">                        
                                    
            </div>
              <br> <br>                         
            
          </div><!-- end row -->
            <div class="modal-footer">               
              <button type="button" id="btnSave" onclick="save()" class="btn btn-info waves-effect waves-light">Simpan</button> 
            </div> 
        </div> 
    </div>
</div>

<!-- end modal -->        

    </body>
</html>                            