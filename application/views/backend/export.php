<!DOCTYPE html>
<html><head>
<link rel="shortcut icon" href="<?=base_url('assets/frontend/images/X.ico');?>">
  
<title>Data Diagnosa ></title>
  
	<style>
	  
		table{
		      
		border-collapse: collapse;
		      
		/*width: 100%;*/
		      
		/*margin: 0 auto;*/
		  
		}
		  
		table th{
		      
		border:1px solid #000;
		      
		padding: 3px;
		      
		font-weight: bold;
		      
		text-align: center;
		  
		}
		  
		table td{
		      
		border:1px solid #000;
		      
		padding: 3px;
		      
		vertical-align: top;
		  
		}
	</style>
</head><body>
<!-- kop -->
<h2 style="text-align: center; font-weight: bold;">Apotek Anwad Farma</h2>
<p style="text-align: center;font-weight: bold;">Jl. Abd. Daeng Sirua No. 66 </p>
<p style="text-align: center;">Buka:  Senin - Kamis, Sabtu : 17.00 PM - 22.00 PM, Jumat dan Minggu Tutup</p>
<hr>
<!-- N Kop -->
<p style="text-align: center;font-weight: bold;">Data Diagnosa <?php echo date('d-m-Y', strtotime($tgl1))?> sampai <?php echo date('d-m-Y', strtotime($tgl2))?></p>
<br>
<table>    
    <tr>           
        <th style="width: 20px;">No</th>
        <th style="width: 130px;">Nama Pasien</th>
        <th style="width: 50px;">Jenis Kelamin</th>
        <th style="width: 90px;">No. Telp</th>
        <th style="width: 90px;">Pekerjaan</th>
        <th style="width: 90px;">Penyakit</th>
        <th style="width: 70px;">Tanggal Diagnosa</th>
    </tr>

    <?php $nomor=1; foreach($detail as $row) {?>
        <tr>                                                
           <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nama ?></td>
           <td style="border:1px solid #000;"><?php echo $row->jk ?></td>
           <td style="border:1px solid #000;"><?php echo $row->no_telp ?></td>
           <td style="border:1px solid #000;"><?php echo $row->pekerjaan ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nama_penyakit ?></td>
           <td style="border:1px solid #000;"><?php echo date('d-m-Y', strtotime($row->tanggal))?></td>
        </tr>   
    <?php $nomor++;}?> 
</table>
<!-- //	 -->
</body></html>