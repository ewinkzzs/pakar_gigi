
                <!-- Page Inner -->
                <div class="page-inner">
                    
                    <div id="main-wrapper">
                        <div class="row">
                            <div class="col-md-12">                            
                                <div class="panel panel-white">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Data Sistem</h4>
                                    </div>
                                    <div class="panel-body">
                                        
                                      <div class="col-md-3">
                                          <div class="panel panel-white stats-widget">
                                              <div class="panel-body">
                                                  <div class="pull-left">
                                                      <span class="stats-number"><?php echo $penyakit->jumlah; ?></span>
                                                      <p class="stats-info">Data Penyakit</p>
                                                  </div>
                                                  <div class="pull-right">
                                                      <i class="icon-arrow_upward stats-icon"></i>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="col-md-3">
                                          <div class="panel panel-white stats-widget">
                                              <div class="panel-body">
                                                  <div class="pull-left">
                                                      <span class="stats-number"><?php echo $gejala->jumlah; ?></span>
                                                      <p class="stats-info">Data Gejala</p>
                                                  </div>
                                                  <div class="pull-right">
                                                      <i class="icon-arrow_upward stats-icon"></i>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="col-md-3">
                                          <div class="panel panel-white stats-widget">
                                              <div class="panel-body">
                                                  <div class="pull-left">
                                                      <span class="stats-number"><?php echo $solusi->jumlah; ?></span>
                                                      <p class="stats-info">Data Solusi</p>
                                                  </div>
                                                  <div class="pull-right">
                                                      <i class="icon-arrow_upward stats-icon"></i>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="col-md-3">
                                          <div class="panel panel-white stats-widget">
                                              <div class="panel-body">
                                                  <div class="pull-left">
                                                      <span class="stats-number"><?php echo $pasien->jumlah; ?></span>
                                                      <p class="stats-info">Data Pasien</p>
                                                  </div>
                                                  <div class="pull-right">
                                                      <i class="icon-arrow_upward stats-icon"></i>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div><!-- Row -->
                    </div><!-- Main Wrapper -->
                    <div class="page-footer">
                        <p>© Copyright by STMIK HANDAYANI</p>
                    </div>
                </div><!-- /Page Inner -->
                
            </div><!-- /Page Content -->
        </div><!-- /Page Container -->
    </body>
</html>                                  
<!-- Javascripts -->
        <script src="<?php echo base_url(); ?>assets_admin/plugins/jquery/jquery-3.1.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/uniform/js/jquery.uniform.standalone.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/switchery/switchery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/datatables/js/jquery.datatables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/js/space.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/js/pages/table-data.js"></script>