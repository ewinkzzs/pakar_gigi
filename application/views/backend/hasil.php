
            

                <div class="page-inner">
                    
                    <div id="main-wrapper">
                        <div class="row">
                            <div class="col-md-12">                            
                                <div class="panel panel-white">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Data Hasil Diagnosa</h4>
                                        <button type="button" class="btn btn-success m-b-sm" onclick="add_()" data-toggle="modal" data-target="#con-close-modal"><i class="glyphicon glyphicon-print"></i> Export</button>    
                                        <!-- <button><a href="<?php echo base_url(); ?>admin/formexport">Export</a></button> -->
                                    </div>
                                    <div class="panel-body">
                                        
                                        <!-- Modal -->
                                        
                                        <div class="table-responsive">
                                            <table id="example3" class="display table" style="width: 100%; cellspacing: 0;">
                                                <thead>
                                                    <tr>
                                                        <th style="width:50px;"><b>No</b></th>
                                                        <th><b>Nama Pasien</b></th>
                                                        <th><b>Jenis Kelamin</b></th>
                                                        <th><b>Penyakit</b></th>                
                                                        <!-- <th><b>CF</b></th>                 -->
                                                        <th><b>Tanggal Diangosa</b></th>                
                                                        <!-- <th style="width:125px;"><b>Aksi</b></th> -->
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th style="width:50px;"><b>No</b></th>
                                                        <th><b>Nama Pasien</b></th>
                                                        <th><b>Jenis Kelamin</b></th>
                                                        <th><b>Penyakit</b></th>                
                                                        <!-- <th><b>CF</b></th>                 -->
                                                        <th><b>Tanggal Diangosa</b></th>                
                                                        <!-- <th style="width:125px;"><b>Aksi</b></th> -->
                                                    </tr>
                                                </tfoot>
                                                <tbody>
                                                    <?php $no=1; foreach ($hasil as $r) { ?>
                                                    <tr>
                                                        <td><?php echo $no;?></td>                                               
                                                        <td><?php echo $r->nama;?></td>
                                                        <td><?php echo $r->jk;?></td> 
                                                        <td><?php echo $r->nama_penyakit;?></td>                   
                                                        <td><?php echo $r->waktu;?></td>                              
                                                        <!-- <td>
                                                            <button class="btn btn-success" onclick="edit_penyakit(<?php echo $r->id;?>)"><i class="glyphicon glyphicon-pencil"></i></button>                                                    
                                                            <button class="btn btn-danger" onclick="delete_penyakit(<?php echo $r->id;?>)"><i class="glyphicon glyphicon-remove"></i></button>
                                                        </td>    -->
                                                    </tr>
                                                    <?php $no++; } ?>
                                                    
                                                </tbody>                                        
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Row -->
                    </div><!-- Main Wrapper -->
                    <div class="page-footer">
                        <p>© Copyright by STMIK HANDAYANI</p>
                    </div>
                </div><!-- /Page Inner -->
                
            </div><!-- /Page Content -->
        </div><!-- /Page Container -->
        
        
        <!-- Javascripts -->
        <script src="<?php echo base_url(); ?>assets_admin/plugins/jquery/jquery-3.1.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/uniform/js/jquery.uniform.standalone.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/switchery/switchery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/datatables/js/jquery.datatables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/js/space.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/js/pages/table-data.js"></script>
    </body>
</html>

<script type="text/javascript">
        
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
    var save_method; //for save method string
    var table;
 
     
    function add_()
    {
      $save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show');
    }

    function save()
    {
      var 
        url,
        tgl_1 = document.getElementById('tgl_1').value,
        tgl_2 = document.getElementById('tgl_2').value;
        base_url = "<?php echo base_url();?>";
        window.location.href = base_url+"riwayat/export/" + tgl_1 +"/"+tgl_2;

    }

 
  </script>                                    

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;" >
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 class="modal-title">Export Hasil Diagnosa</h4> 
            </div> 
          <form class="class="form-appointment ui-form" action="#" id="form" onsubmit="return Validation()" name="frmOnline"> <!-- target="_blank" -->
          <div class="row">
            <div class="col-lg-10 col-md-offset-1">           
              <input type="hidden" value="" name="id"/>
              <div class="form-group ">
                   <!-- ,dosis,aturan_pakai,ket -->
                  <div class="col-xs-12">
                    <label for="form_control_1">Tanggal Mulai</label>
                    <input class="form-control" type="date" name="tgl_1" id="tgl_1">     
                    </div>
              </div>  
              <br> <br>                  
              <br>                  
              <div class="form-group ">
                  <div class="col-xs-12">
                    <label for="form_control_1">Sampai Tanggal</label>
                    <input class="form-control" type="date" name="tgl_2" id="tgl_2">
                    </div>
              </div>       
              <br> <br>                      

            <div class="modal-footer">               
              <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Export</button> 
              <!-- <button type="submit" target="_blank" class="btn btn-success">Export</button>  -->
            </div> 
            </div> 

        </div> 
        </form>
    </div>
</div>                                    