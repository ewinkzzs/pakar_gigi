
            

                <div class="page-inner">
                    
                    <div id="main-wrapper">
                        <div class="row">
                            <div class="col-md-12">                            
                                <div class="panel panel-white">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Data Solusi</h4>
                                        <button type="button" class="btn btn-success m-b-sm" onclick="add_()" data-toggle="modal" data-target="#con-close-modal"><i class="glyphicon glyphicon-plus"></i> Tambah</button>                                   
                                    </div>
                                    <div class="panel-body">
                                        
                                        <!-- Modal -->
                                        
                                        <div class="table-responsive">
                                            <table id="example3" class="display table" style="width: 100%; cellspacing: 0;">
                                                <thead>
                                                    <tr>
                                                        <th style="width:50px;"><b>No</b></th>
                                                        <th><b>Nama Penyakit</b></th>                
                                                        <th><b>Definisi</b></th>                
                                                        <th><b>Solusi</b></th>                 
                                                        <th style="width:125px;"><b>Aksi</b></th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th><b>No</b></th>
                                                        <th><b>Nama Penyakit</b></th>                
                                                        <th><b>Definisi</b></th>                
                                                        <th><b>Solusi</b></th>                
                                                        <th style="width:125px;"><b>Aksi</b></th>
                                                    </tr>
                                                </tfoot>
                                                <tbody>
                                                    <?php $no=1; foreach ($solusi as $r) { ?>

                                                    <tr>
                                                        <td><?php echo $no;?></td>                                               
                                                        <td><?php echo $r->nama_penyakit;?></td>
                                                        <td><?php echo $r->defenisi;?></td>
                                                        <td><?php echo $r->solusi;?></td>                              
                                                        <td>
                                                            <button class="btn btn-success" onclick="edit_(<?php echo $r->id;?>)"><i class="glyphicon glyphicon-pencil"></i></button>                                                    
                                                            <button class="btn btn-danger" onclick="delete_(<?php echo $r->id;?>)"><i class="glyphicon glyphicon-remove"></i></button>
                                                        </td>   
                                                    </tr>
                                                    <?php $no++; } ?>
                                                    
                                                </tbody>                                        
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Row -->
                    </div><!-- Main Wrapper -->
                    <div class="page-footer">
                        <p>© Copyright by STMIK HANDAYANI</p>
                    </div>
                </div><!-- /Page Inner -->
                
            </div><!-- /Page Content -->
        </div><!-- /Page Container -->
        
        
        <!-- Javascripts -->
        <script src="<?php echo base_url(); ?>assets_admin/plugins/jquery/jquery-3.1.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/uniform/js/jquery.uniform.standalone.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/switchery/switchery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/datatables/js/jquery.datatables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/js/space.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_admin/js/pages/table-data.js"></script>
    </body>
</html>

<script type="text/javascript">
        
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
    var save_method; //for save method string
    var table;
 
     
    function add_()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }

    function edit_(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('admin/solusi_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="id_penyakit"]').val(data.id_penyakit);
            $('[name="defenisi"]').val(data.defenisi);
            $('[name="solusi"]').val(data.solusi);

            $('#con-close-modal').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Ubah Data Solusi'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }
  
    function save()
    {
      var url;
        var id_penyakit = document.frmOnline.id_penyakit; 
        var defenisi = document.frmOnline.defenisi;
        var solusi = document.frmOnline.solusi;


        if (id_penyakit.value == "") {
            alert("Penyakit Tidak Boleh Kosong");
            id_penyakit.focus();
            return false;
        }
        if (defenisi.value == "") {
            alert("Defenisi Tidak Boleh Kosong");
            defenisi.focus();
            return false;
        }
        if (solusi.value == "") {
            alert("Solusi Tidak Boleh Kosong");
            solusi.focus();
            return false;
        }
      /////
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('admin/solusi_add')?>";
      }
      else
      {
        url = "<?php echo site_url('admin/solusi_update')?>";
      }
 
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               alert('Data berhasil disimpan');
               $('#modal_form').modal('hide');
              location.reload();// for reload a page
              
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_(id)
    {
      if(confirm('Anda Yakin ingin menghapus data ini ?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('admin/solusi_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

      }
    }
 
  </script>                                    

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 class="modal-title">Masukkan Solusi</h4> 
            </div> 
          <form class="form-appointment ui-form" action="#" id="form" onsubmit="return Validation()" name="frmOnline">
          <div class="row">
            <div class="col-lg-10 col-md-offset-1">    
              <br>          
              <input type="hidden" value="" name="id"/>
              <div class="form-group ">
                   <!-- ,dosis,aturan_pakai,ket -->
                  <div class="col-xs-12">
                    <label for="form_control_1">Pilih Penyakit
                    </label>
                    <select class="form-control" name="id_penyakit" id="id_penyakit">
                      <option value="">Pilih</option>
                      <?php 
                          $penyakit=$this->Penyakit_model->get_all(); 
                          foreach ($penyakit as $r) {
                      ?>
                        <option value="<?php echo $r->id_penyakit ?>"><?php echo $r->nama_penyakit; ?></option>
                      <?php } ?>
                    </select>
                    </div>
              </div>  
              <br> <br> <br>                 
              <div class="form-group ">
                  <div class="col-xs-12">
                    <label for="form_control_1">Defenisi</label>
                    <textarea class="form-control" rows="4" cols="50" name="defenisi" placeholder="Definisi" id="defenisi"></textarea>
                    </div>
              </div>       
                
              <div class="form-group ">
                  <div class="col-xs-12">
                     <br> 
                    <label for="form_control_1">Solusi</label>
                    <textarea class="form-control" rows="4" cols="50" name="solusi" placeholder="Solusi" id="solusi"></textarea>
                    </div>
              </div>   
              <br><br>                    
            </div>
          </div><!-- end row -->
            <div class="modal-footer">               
              <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Simpan</button> 
            </div> 
        </div> 
    </div>
</div>                                    