 <!DOCTYPE html>
<html>
    <head> 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Learn PHP CodeIgniter Framework with AJAX and Bootstrap</title>
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
    <!-- batas -->
    
    
  </head>
  <body>
 
 
  <div class="container">
    <h1>Learn PHP CodeIgniter Framework with AJAX and Bootstrap</h1>
</center>
    <h3>Mahasiswa</h3>
    <br />
    <button class="btn btn-success" onclick="add_Mhs()"><i class="glyphicon glyphicon-plus"></i> Add Mahasiswa</button>
    <br />
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
      <thead>
        <tr>
					<th>ID</th>
					<th>NPM</th>
					<th>Nama Panggilan</th>
					<th>Nama Lengkap</th>
					<th>Jenis Kelamin</th>
          <th>Alamat</th>
          <th>Nama Facebook</th>
          <th>ANgkatan</th>
          <th>No. Telepon</th>
          <th style="width:125px;">Action
          </p></th>
        </tr>
      </thead>
      <tbody>
				<?php foreach($anggotas as $mhs){?>
				     <tr>
				         <td><?php echo $mhs->id;?></td>
                 <td><?php echo $mhs->npm;?></td>
				         <td><?php echo $mhs->nmPanggil;?></td>
								 <td><?php echo $mhs->nmLengkap;?></td>
								<td><?php echo $mhs->jk;?></td>
								<td><?php echo $mhs->alamat;?></td>
                <td><?php echo $mhs->nmFb;?></td>
                <td><?php echo $mhs->angkatan;?></td>
                <td><?php echo $mhs->noTelp;?></td>
								<td>
									<button class="btn btn-warning" onclick="edit_mhs(<?php echo $mhs->id;?>)"><i class="glyphicon glyphicon-pencil"></i></button>
									<button class="btn btn-danger" onclick="delete_mhs(<?php echo $mhs->id;?>)"><i class="glyphicon glyphicon-remove"></i></button>
 
 
								</td>
				      </tr>
				     <?php }?>
 
 
 
      </tbody>
 
      <tfoot>
        
      </tfoot>
    </table>
 
  </div>
 
    <script src="<?php echo base_url(); ?>assets/jquery/jquery-3.1.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/js/dataTables.bootstrap.js"></script>
 
 
  <script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
    var save_method; //for save method string
    var table;
 
     
    function add_Mhs()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }
 
    function edit_mhs(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
 
      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('index.php/Mahasiswa/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
        // npm,nmPanggil,nmLengkap,jk,alamat,nmFb,angkatan,noTelp
            $('[name="id"]').val(data.id);
            $('[name="npm"]').val(data.npm);
            $('[name="nmPanggil"]').val(data.nmPanggil);
            $('[name="nmLengkap"]').val(data.nmLengkap);
            $('[name="jk"]').val(data.jk);
            $('[name="alamat"]').val(data.alamat);
            $('[name="nmFb"]').val(data.nmFb);
            $('[name="angkatan"]').val(data.angkatan);
            $('[name="noTelp"]').val(data.noTelp); 
 
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Mahaiswa'); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }
 
 
 
    function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('index.php/Mahasiswa/Mhs_add')?>";
      }
      else
      {
        url = "<?php echo site_url('index.php/Mahasiswa/Mhs_update')?>";
      }
 
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
              location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }
 
    function delete_mhs(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('index.php/Mahasiswa/mhs_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
 
      }
    }
 
  </script>
 
  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Mahasiswa Form</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">

        <!-- npm,nmPanggil,nmLengkap,jk,alamat,nmFb,angkatan,noTelp -->
          <input type="hidden" value="" name="id"/>
          <div class="form-body">

            <div class="form-group">
              <label class="control-label col-md-3">NPM</label>
              <div class="col-md-9">
                <input name="npm" placeholder="NPM" class="form-control" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Nama Panggilan</label>
              <div class="col-md-9">
                <input name="nmPanggil" placeholder="Nama Panggilan" class="form-control" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Nama Lengkap</label>
              <div class="col-md-9">
								<input name="nmLengkap" placeholder="Nama Lengkap" class="form-control" type="text">
              </div>
            </div>

						<div class="form-group">
							<label class="control-label col-md-3">Jenis Kelamin</label>
							<div class="col-md-9">
								<input name="jk" placeholder="Jenis Kelamin" class="form-control" type="text">
							</div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Alamat</label>
              <div class="col-md-9">
                <input name="alamat" placeholder="Alamat" class="form-control" type="text">
              </div>
						</div>

            <div class="form-group">
              <label class="control-label col-md-3">Nama Facebook</label>
              <div class="col-md-9">
                <input name="nmFb" placeholder="Nama Facebook" class="form-control" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Angkatan</label>
              <div class="col-md-9">
                <input name="angkatan" placeholder="Angkatan" class="form-control" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">No. Telepon</label>
              <div class="col-md-9">
                <input name="noTelp" placeholder="No. Telepon" class="form-control" type="text">
              </div>
            </div>
 
          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->

  </body>
</html>