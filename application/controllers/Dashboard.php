<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
        parent::__construct();
        // $this->simple_login->cek_login();
		$this->load->helper('url');               
	 	$this->load->model('Solusi_model');
	} 

	public function index()
	{
        $data['penyakit']=$this->Solusi_model->get_all();
		$this->load->view('frontend/header');
		$this->load->view('frontend/dashboard', $data);
		$this->load->view('frontend/footer');
		
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */