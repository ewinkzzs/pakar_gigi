<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->simple_login->cek_login();
        $this->load->helper(array('form', 'url', 'date'));
	 	$this->load->model('Penyakit_model');
	 	$this->load->model('Beranda_model');
                $this->load->model('Gejala_model');
                $this->load->model('Solusi_model');
                $this->load->model('Rule_model');
                $this->load->model('User_model');
                $this->load->model('Diagnosa_model');
                $this->load->model('Hasil_model');
                $this->load->library('dompdf_gen');
	}

	public function index() { 
                $data['penyakit'] = $this->Beranda_model->get_count_penyakit(); 
                $data['gejala'] = $this->Beranda_model->get_count_gejala(); 
                $data['solusi'] = $this->Beranda_model->get_count_solusi(); 
                $data['pasien'] = $this->Beranda_model->get_count_pasien();              
                $this->load->view('backend/header');
                $this->load->view('backend/dashboard',$data);  
        }

	public function penyakit()
        	{	
        		$data['penyakit']=$this->Penyakit_model->get_all();
                        $this->load->view('backend/header');                
                        $this->load->view('backend/penyakit',$data); 
        	}

	public function penyakit_add() 
                {                        
                        $data = array(                         
                                        'kd_penyakit' => $this->input->post('kd_penyakit'),
                                        'nama_penyakit' => $this->input->post('nama_penyakit'),                             
                                );
                        $insert = $this->Penyakit_model->penyakit_add($data);
                        echo json_encode(array("status" => TRUE));
                }

        public function penyakit_edit($id)
                {
                        $data = $this->Penyakit_model->get_by_id($id);
                        echo json_encode($data);
                }
        public function penyakit_update()
                {
                $data = array(
                                'kd_penyakit' => $this->input->post('kd_penyakit'),
                                'nama_penyakit' => $this->input->post('nama_penyakit'), 
                        );
                $this->Penyakit_model->penyakit_update(array('id_penyakit' => $this->input->post('id_penyakit')), $data);
                echo json_encode(array("status" => TRUE));
                }
        public function penyakit_delete($id)
                {
                        $this->Penyakit_model->delete_by_id($id);
                        echo json_encode(array("status" => TRUE));
                }
        public function gejala() 
        {                  
                $data['gejala']=$this->Gejala_model->get_all();
                $this->load->view('backend/header');                
                $this->load->view('backend/gejala',$data);                                         
        }

        public function gejala_add() 
                {                        
                        $data = array(                         
                                        'kd_gejala' => $this->input->post('kd_gejala'),
                                        'gejala' => $this->input->post('gejala'),                             
                                );
                        $insert = $this->Gejala_model->gejala_add($data);
                        echo json_encode(array("status" => TRUE));
                }

        public function gejala_edit($id)
                {
                        $data = $this->Gejala_model->get_by_id($id);
                        echo json_encode($data);
                }
        public function gejala_update()
                {
                $data = array(
                                'kd_gejala' => $this->input->post('kd_gejala'),
                                'gejala' => $this->input->post('gejala'), 
                        );
                $this->Gejala_model->gejala_update(array('id_gejala' => $this->input->post('id_gejala')), $data);
                echo json_encode(array("status" => TRUE));
                }

        public function gejala_delete($id)
                {
                        $this->Gejala_model->delete_by_id($id);
                        echo json_encode(array("status" => TRUE));
                }

        public function solusi() 
        {                  
                $data['solusi']=$this->Solusi_model->get_all();
                $this->load->view('backend/header');                
                $this->load->view('backend/solusi',$data);                                         
        }

        public function solusi_add() 
                {                        
                        $data = array(                         
                                        'id_penyakit' => $this->input->post('id_penyakit'),
                                        'defenisi' => $this->input->post('defenisi'),                             
                                        'solusi' => $this->input->post('solusi'),                             
                                );
                        $insert = $this->Solusi_model->add_($data);
                        echo json_encode(array("status" => TRUE));
                }

        public function solusi_edit($id)
                {
                        $data = $this->Solusi_model->get_by_id($id);
                        echo json_encode($data);
                }
        public function solusi_update()
                {
                $data = array(
                                'id_penyakit' => $this->input->post('id_penyakit'),
                                'defenisi' => $this->input->post('defenisi'),                             
                                'solusi' => $this->input->post('solusi'),  
                        );
                $this->Solusi_model->update_(array('id' => $this->input->post('id')), $data);
                echo json_encode(array("status" => TRUE));
                }

        public function solusi_delete($id)
                {
                        $this->Solusi_model->delete_($id);
                        echo json_encode(array("status" => TRUE));
                }

        public function rule() 
        {                  
                $data['rule']=$this->Rule_model->get_all();
                $data['penyakit']=$this->Penyakit_model->get_all();
                $this->load->view('backend/header');                
                $this->load->view('backend/rule',$data);                                         
        }

        public function rule_add() 
                {                        
                        $data = array(                         
                                'kd_gejala' => $this->input->post('kd_gejala'),                             
                                'kd_penyakit' => $this->input->post('kd_penyakit'),
                                'md' => $this->input->post('md'),
                                'mb' => $this->input->post('mb'),
                                );
                        $insert = $this->Rule_model->add_($data);
                        echo json_encode(array("status" => TRUE));
                }

        public function rule_edit($id)
                {
                        $data = $this->Rule_model->get_by_id($id);
                        echo json_encode($data);
                }
        public function rule_update()
                {
                $data = array(
                                'kd_gejala' => $this->input->post('kd_gejala'),                             
                                'kd_penyakit' => $this->input->post('kd_penyakit'),
                                'md' => $this->input->post('md'),
                                'mb' => $this->input->post('mb'), 
                        );
                $this->Rule_model->update_(array('id' => $this->input->post('id')), $data);
                echo json_encode(array("status" => TRUE));
                }

        public function rule_delete($id)
                {
                        $this->Rule_model->delete_($id);
                        echo json_encode(array("status" => TRUE));
                }

        public function user() 
        {                  
                $data['user']=$this->User_model->get_all();
                $this->load->view('backend/header');                
                $this->load->view('backend/user',$data);                                         
        }

        public function user_add() 
                {                        
                        $data = array(                                  
                                        'nik' => $this->input->post('nik'),
                                        'nama' => $this->input->post('nama'),
                                        'email' => $this->input->post('email'),
                                        'username' => $this->input->post('username'),
                                        'password' => md5($this->input->post('password')),
                                        'level' => md5($this->input->post('level')),
                                        'aktif' => '1',                         
                                );
                        $insert = $this->User_model->add_($data);
                        echo json_encode(array("status" => TRUE));
                }

        public function user_edit($id)
                {
                        $data = $this->User_model->get_by_id($id);
                        echo json_encode($data);
                }
        public function user_update()
                {
                        $data = array(
                                        'nik' => $this->input->post('nik'),
                                        'nama' => $this->input->post('nama'),
                                        'email' => $this->input->post('email'),
                                        'username' => $this->input->post('username'),
                                        'password' => md5($this->input->post('password')),
                        );
                        $this->User_model->update_(array('id' => $this->input->post('id')), $data);
                        echo json_encode(array("status" => TRUE));
                }

        public function user_delete($id)
                {
                        $this->User_model->delete_($id);
                        echo json_encode(array("status" => TRUE));
                }

        public function hasil() 
        {                  
                $data['hasil']=$this->Diagnosa_model->get_hasil();
                $this->load->view('backend/header');                
                $this->load->view('backend/hasil',$data);                                         
        }

        public function form_export() 
        {                  
                $this->load->view('backend/header');                
                $this->load->view('backend/formexport');                                         
        }

        public function export($tgl1,$tgl2){
        
            $data['title'] ='Data Diagnosa';
            //judul title
            // $tgl1=$this->input->post('tgl_1');
            // $tgl2=$this->input->post('tgl_2');
            
            $data['tgl1']=$tgl1;        
            $data['tgl2']=$tgl2;        
            $data['detail']=$this->Hasil_model->get_export($tgl1,$tgl2);
            //query model semua barang
             
                    
            $this->load->view('backend/export',$data);
             
                    
            $paper_size='A4';
            //paper size
                    
            $orientation='portrait';
            //tipe format kertas
                    
            $html=$this ->output->get_output();
             
                    
            $this->dompdf->set_paper($paper_size,$orientation);
                    
            //Convert to PDF
                    
            $this->dompdf->load_html($html);            
            $this->dompdf->render();   
            $this->dompdf->stream("Data-diagnosa.pdf" ,
                array('Attachment'=>0));
                
        } 

}

/* End of file Adm in.php */
/* Location: ./application/controllers/Admin.php */