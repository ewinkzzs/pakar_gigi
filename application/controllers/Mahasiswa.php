<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {  

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('Mhs_model');
	 	}

	public function index()
		{
			$data['anggotas']=$this->Mhs_model->get_all_mhs();
			$this->load->view('Mhs_view',$data);	
		}

	public function Mhs_add() 
		{
			// npm,nmPanggil,nmLengkap,jk,alamat,nmFb,angkatan,noTelp
			$data = array(
					'npm' => $this->input->post('npm'),
					'nmPanggil' => $this->input->post('nmPanggil'),
					'nmLengkap' => $this->input->post('nmLengkap'),
					'jk' => $this->input->post('jk'),
					'alamat' => $this->input->post('alamat'),
					'nmFb' => $this->input->post('nmFb'),
					'angkatan' => $this->input->post('angkatan'),
					'noTelp' => $this->input->post('noTelp'),
				);
			$insert = $this->Mhs_model->mhs_add($data);
			echo json_encode(array("status" => TRUE));
		}

	public function ajax_edit($id)
		{
			$data = $this->Mhs_model->get_by_id($id);
 
 
 
			echo json_encode($data);
		}

	public function Mhs_update()
	{
		$data = array(
				'npm' => $this->input->post('npm'),
					'nmPanggil' => $this->input->post('nmPanggil'),
					'nmLengkap' => $this->input->post('nmLengkap'),
					'jk' => $this->input->post('jk'),
					'alamat' => $this->input->post('alamat'),
					'nmFb' => $this->input->post('nmFb'),
					'angkatan' => $this->input->post('angkatan'),
					'noTelp' => $this->input->post('noTelp'),
			);
		$this->Mhs_model->Mhs_update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function Mhs_delete($id)
	{
		$this->Mhs_model->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}


}