<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diagnosa extends CI_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
            $this->simple_login->cek_login();
			$this->load->helper('url');
			$this->load->helper(array('form', 'url', 'date'));
			$this->load->library('form_validation');
			$this->load->model('Gejala_model');
			$this->load->model('Diagnosa_model');
	 		// $this->load->model('Mhs_model');
	 	}

	public function index()
		{
		if(!$this->input->post("gejala"))
	    {
            $data['gejala'] = $this->Gejala_model->get_all(); 
			$this->load->view('frontend/header');
			$this->load->view('frontend/diagnosa', $data);
			$this->load->view('frontend/footer');
		} else {

            $gejala = implode(",", $this->input->post("gejala"));
            // var_dump($gejala);

            if (strlen($gejala) >= 4) {
                $data['gejala'] = implode(",", $this->input->post("gejala"));
                $this->Diagnosa_model->tmp_gejala_delete();
                $this->Diagnosa_model->tmp_penyakit_delete();

                $data["listGejala"] = $this->Diagnosa_model->get_list_by_id($gejala);
                // $data['tb_obat']=$this->Obat_model->get_all();
                // $data['anjuran']=$this->Tips_model->get_where_anjuran();
                // $data['hindari']=$this->Tips_model->get_where_hindari(); 

                // isi tmt gejala
                $selectors  = htmlentities(implode(',',$this->input->post("gejala")));
                $data2=$selectors;
                $input = $data2;
                          //memecahkan string input berdasarkan karakter '\r\n\r\n'
                $pecah = explode("\r\n\r\n", $input);
                          // string kosong inisialisasi
                $text = "";
                          //untuk setiap substring hasil pecahan, sisipkan <p> di awal dan </p> di akhir
                          // lalu menggabungnya menjadi satu string untuk $text
                for ($i=0; $i<=count($pecah)-1; $i++)
                    {
                        $part = str_replace($pecah[$i], "<p>".$pecah[$i]."</p>", $pecah[$i]);
                        $text .=$part;
                    }
                $text_line=$data2;
                $text_line = explode(",",$text_line);
                $posisi=0;
                for ($start=0; $start < count($text_line); $start++) {
                    $baris=$text_line[$start]; 
                    // untuk nilai bobot    
                    //$bobot=substr($databobot,$posisi,1); echo $bobot. "<br>";
                    $isi = array(                  
                                    'id_gejala' => $baris,              
                                 );
                    $insert= $this->Diagnosa_model->tmp_gejala_add($isi);                
                    $posisi++;  
                // batas isi tmt_gejala
                  
                }
                $this->load->view('frontend/header');
                $this->load->view('frontend/hasil_diagnosa',$data);
                $this->load->view('frontend/footer'); 
            } else {
                // $data['gejala'] = $this->Gejala_model->get_all(); 
                $this->load->view('frontend/header');
                $this->load->view('frontend/error_diagnosa');
                $this->load->view('frontend/footer');
                // echo "<script type='text/javascript'>alert('Gejala yang dipilih minimal satu Gejala');</script>";
            }

            

		}

	}

}

/* End of file Diagnosa.php */
/* Location: ./application/controllers/Diagnosa.php */